import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {fromJS} from 'immutable';
import AutoComplete from 'material-ui/AutoComplete';
import MenuItem from 'material-ui/MenuItem';
import {browserHistory} from 'react-router';

const CUSTOMER = "CUSTOMER";

class Search extends Component {
    constructor(props) {
        super(props);
        this.state = { value: "" };
    }

    searchResultsForCustomers() {
        return this.props.customers.map((customer) => {
            let text = customer.get('firstName').trim() + " " + customer.get('lastName').trim();

            return {
                text: text,
                value: this.searchResultToMarkup(CUSTOMER, customer.get('firstName'),
                    customer.get('lastName'),
                    customer.get('cellphoneNumber'),
                    customer.get('emailAddress'),
                    undefined,
                    customer.get('username'))
            }
        })
    }

    hasCustomerResults() {
        return this.props.customers && this.props.customers.first();
    }

    selectCustomer(leadId, name, surname, contactNumber, email, username) {
        this.props.setCurrentCustomer(
            leadId,
            name,
            surname,
            contactNumber,
            email,
            username
        );

        if (this.props.currentLocation === 'event_planner/add-customer') {
            browserHistory.goBack();
        }
    }

    searchResults() {
        let results = fromJS({});
        if (!this.hasCustomerResults()) {
            const nothingFound = [{
                text: "could not find any guests under that phrase",
                value: "could not find any guests under that phrase"
            }];
            return nothingFound;
        }

        if (this.hasCustomerResults()) {
            results = results.merge(this.searchResultsForCustomers());
        }
        return results.toList().toJS();
    }

    searchResultToMarkup(searchResultType, name, surname, contactNumber, email, leadId, username) {
        const identification = searchResultType + " " + name + " " + surname + " " + email;
        return <MenuItem
            primaryText= {identification}
            onClick={() => { this.selectCustomer(leadId, name, surname, contactNumber, email, username) } }
            style={{ fontSize: '14px' }}
            />
    }

    updateSearchRequest(value) {
        this.setState({ value: value });
        this.props.fetchCustomer(value);
    }

    render() {
        const dataSource = this.searchResults();
        return (
            <div className="customer-search">
                <AutoComplete
                    hintText="Search firstname, lastname or email address"
                    dataSource={dataSource}
                    onUpdateInput={this.updateSearchRequest.bind(this) }
                    floatingLabelText="Search guestlist"
                    filter={AutoComplete.fuzzyFilter}
                    open={false}
                    openOnFocus={true}
                    fullWidth={true}
                    />
            </div>
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
    }
}

function mapDispatchToProps(dispatch) {
    let bindings = bindActionCreators({  }, dispatch)
    return bindings;
}

export default connect(mapStateToProps, mapDispatchToProps)(Search);
