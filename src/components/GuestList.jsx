import React, {Component, PropTypes} from 'react';

import {List, ListItem, MakeSelectable} from 'material-ui/List';
import Divider from 'material-ui/Divider';
import Subheader from 'material-ui/Subheader';
import AvatarCircle from 'material-ui/svg-icons/image/lens';

let SelectableList = MakeSelectable(List);

function wrapState(ComposedComponent) {
    return class SelectableList extends Component {
        componentWillMount() {
            this.setState({
                selectedIndex: this.props.defaultValue,
            });
        }
        constructor(props, state) {
            super(props, state);
            this.state = { selectedIndex: this.props.defaultValue, }
            this.handleRequestChange = this.handleRequestChange.bind(this);
        }
        handleRequestChange(event, index) {
            this.setState({
                selectedIndex: index,
            });
            this.props.onChange(event, index);
        }

        render() {
            return (
                <ComposedComponent
                    value={this.state.selectedIndex}
                    onChange={this.handleRequestChange}>
                    {this.props.children}
                </ComposedComponent>
            );
        }
    };
    SelectableList.PropTypes = {
        children: PropTypes.node.isRequired,
        defaultValue: PropTypes.number.isRequired,
    };
}
SelectableList = wrapState(SelectableList);

class GuestList extends Component {
    handleSelect(event, value) {
        return this.props.onChange(event, value);
    }
    deriveToString(value) {
        if (List.isList(value)) {
            return value.toJSON().toString();
        }
        return value.toString();
    }

    render() {

        const iconStyle = {
            color: '#fff',
            marginTop: '-5px',
            float: 'left',
            paddingRight: '5px'
        }
        const Avatar = () => {
            return <AvatarCircle style={iconStyle}/>
        }

        let count = 0;
        const records = this.props.records;
        const height = this.props.height;

        const renderGuestList = records.map((record) => {
            count++;
            return <ListItem className="time" value={record.get('id') }
                key={record.get('id') } leftIcon={<Avatar/>} style={{ marginLeft: '5px', color: '#FFF' }}>
                {record.get('first_name') } {' '}
                {record.get('last_name') }
            </ListItem>
        });

        return (<div style={{ maxHeight: height, overflowY: "scroll", overflowX: 'hidden', }}>
            <Divider />
            <SelectableList onChange={this.handleSelect.bind(this) }>
                {renderGuestList}
            </SelectableList>
        </div>
        );
    }
}

export default GuestList;