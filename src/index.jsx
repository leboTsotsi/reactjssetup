import React from 'react';
import ReactDOM from 'react-dom';
import {render} from 'react-dom';
import { Router, Route, hashHistory, browserHistory } from 'react-router'
import { syncHistoryWithStore } from 'react-router-redux'
import injectTapEventPlugin from 'react-tap-event-plugin';
import Root from './containers/Root'
import configureStore from './store/configureStore'

require("!style-loader!css-loader!sass-loader!./punk.scss");

const store = configureStore();
const history = syncHistoryWithStore(browserHistory, store);
const browserHandler = {
  default: (browser) => alert('Running on ' + { browser })
};

injectTapEventPlugin();

if (module.hot) {
  module.hot.decline("./routes.jsx");
}
render(<Root store={store} history={history} />, document.getElementById('punk-app'))
