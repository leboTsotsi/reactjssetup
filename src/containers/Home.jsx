import React, {PropTypes} from 'react';
import ReactDOM from 'react-dom';

import {connect} from 'react-redux';
import { browserHistory } from 'react-router';
import { bindActionCreators } from 'redux';
import { fromJS } from 'immutable';

class Home extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {

        let heroText = "Don't do it yourself. Let us make your life a little easier.";
        return (
            <div>
                {heroText}
            </div>
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        selectedClusters: state.selectedClusters
    }
}

function mapDispatchToProps(dispatch) {
    let bindings = bindActionCreators({ sendToast }, dispatch)
    return bindings;
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);