import React, {PropTypes, Component} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import xmlName from '../assets/guestlist.xml';

import Immutable from 'immutable';
import {List} from 'immutable';

import _ from 'lodash';

import Divider from 'material-ui/Divider'
import ColorManipulator from 'material-ui/utils/colorManipulator';
import Spacing from 'material-ui/styles/spacing';
import zIndex from 'material-ui/styles/zIndex';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import * as Colors from 'material-ui/styles/colors';
import TextField from 'material-ui/TextField';
import {Card} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import AddCircleOutline from 'material-ui/svg-icons/content/add-circle-outline';
import AvatarCircle from 'material-ui/svg-icons/image/lens';
import IconButton from 'material-ui/IconButton';
import {ListUI, ListItem} from 'material-ui/List';
import Subheader from 'material-ui/Subheader';
import CircularProgress from 'material-ui/CircularProgress';
import SearchIcon from 'material-ui/svg-icons/action/search';

import GuestList from '../components/GuestList';
import Search from '../components/Search';

import {isCellInvalid, isEmailInvalid} from '../utilities';

import {loadRecords, addContactNumber} from '../actions/records';

class MainLayout extends React.Component {
  constructor(props, state) {
    super(props, state);
    this.state = {
      firstName: 'None',
      lastName: 'None',
      company: 'None',
      emailAddress: '',
      contactNumber: '',

      firstname_err: '',
      lastname_err: '',
      company_err: '',
      contactnumber_err: '',
      emailaddress_err: '',

      count: 0,
      id: '',
      canSave: true,
    };
  }

  componentWillMount() {
    let anotherVar = Immutable.fromJS(xmlName);
    let records = Immutable.fromJS(anotherVar.get('dataset').get('record'));
    if (List.isList(records)) {
      let jsonRecords = records.toJSON();
      let payloadRecords = new Array();
      var count = 0;
      jsonRecords.map((record) => {
        count++;
        let updatedRecord = {
          id: [count],
          first_name: record.first_name,
          last_name: record.last_name,
          company: record.company,
          contact_number: [''],
          email_address: ['']
        };
        payloadRecords.push(updatedRecord);
      });
      let payload = {
        records: payloadRecords
      };
      this.props.loadRecords(payload);
      this.setState({ count: count });
    }
  }

  deriveToString(value) {
    if (List.isList(value)) {
      return value.toJSON().toString();
    }
    return value.toString();
  }

  handleTextChange(name, e) {
    let change = {};
    change[name] = e.target.value;
    this.setState(change);
  }

  handleListSelect(event, guestId) {
    this.setState({ id: guestId });
    this.setStateByRecordId(guestId);
  }

  setStateByRecordId(id) {
    this.props.guestlist.get('records').map((record) => {
      if (this.deriveToString(id) === this.deriveToString(record.get('id'))) {
        this.setState({
          id: this.deriveToString(id),
          firstName: this.deriveToString(record.get('first_name')),
          lastName: this.deriveToString(record.get('last_name')),
          company: this.deriveToString(record.get('company')),
          emailAddress: this.deriveToString(record.get('email_address'))
        });
      }
    });
  }

  addEmailAddress() {
    let isInvalidEmail;
    if (isInvalidEmail = isEmailInvalid(this.state.emailAddress)) {
      this.setState({ emailaddress_err: "Please enter a valid email" })
    } else {
      this.setState({ emailaddress_err: '', canSave: false });
    }
  }

  saveDetails() {
    this.setState({ canSave: true });
    let testRecords = new Array();

    this.props.guestlist.get('records').map((record) => {
      let identifier = record.get('id');
      let firstName = record.get('first_name');
      let lastName = record.get('last_name');
      let company = record.get('company');
      let contactNumber = record.get('contact_number');
      let emailAddress = record.get('email_address');

      if (this.deriveToString(identifier) === this.state.id) {
        emailAddress = [this.state.emailAddress];
      }

      let payload = {
        id: identifier,
        first_name: firstName,
        last_name: lastName,
        company: company,
        contact_number: contactNumber,
        email_address: emailAddress
      };
      testRecords.push(payload);
    });

    let stateRecords = {
      records: testRecords
    };
    this.props.addContactNumber(stateRecords);
  }

  loading() {
    const doneStyle = { width: 100, height: 400, borderStyle: "solid", borderColor: "#fff", borderRadius: "5em" }
    return (
      <div>
        <center>
          <CircularProgress/>
        </center>
      </div>
    )
  }
  render() {

    const styles = {
      largeIcon: {
        width: 70,
        height: 70,
        marginTop: '20px',
        color: '#FFF',
        float: 'left',
        marginLeft: '10px'
      },

    };

    const Avatar = () => (
      <div>
        <IconButton
          iconStyle={styles.largeIcon}>
          <AvatarCircle />
        </IconButton>
      </div>
    );

    const fieldStyle = {
      width: '85%',
      color: '#288596'
    }

    const iconStyle = {
      color: '#288596',
      marginTop: '8px',
      float: 'left',
      marginLeft: '0px',
    }

    const AddIcon = () => {
      return <AddCircleOutline style={iconStyle}/>
    }

    const errorStyle = {
      textAlign: 'left',
      color: '#85284F',
    }

    const fieldHeadingStyle = {
      width: '97%',
      color: '#288596',
      fontSize: '1em',
      fontWeight: 'bold',
      paddingLeft: '30px',
    }

    const fieldFullNameStyle = {
      width: '97%',
      color: '#288596',
      fontSize: '1.08em',
      fontWeight: 'bold',
      paddingLeft: '95px',
    }

    const fieldCompanyStyle = {
      width: '97%',
      color: '#288596',
      fontSize: '1.08em',
      fontWeight: 'normal',
      paddingLeft: '95px',
    }

    const addNumberButtonStyle = {
      color: '#288596',
      fontSize: '1.08em',
      fontWeight: 'normal',
    }

    const buttonStyle = {
      backgroundColor: "#288596",
      color: '#fff',
      display: 'inline-block'
    }

    const muiTheme = getMuiTheme({
      palette: {
        primary1Color: '#288596',
        primary2Color: Colors.cyan700,
        primary3Color: Colors.lightBlack,
        accent1Color: '#FFF',
        accent2Color: Colors.grey100,
        accent3Color: Colors.grey500,
        textColor: Colors.grey700
      }
    });

    const renderfNameTextField = (hintText, floatinglabel, value, onChangeText, errorText) => {
      return <center>
        <TextField
          hintText={hintText}
          floatingLabelText={floatinglabel}
          className="textField"
          value={value}
          onChange={this.handleTextChange.bind(this, onChangeText) }
          style={fieldStyle}
          errorText={errorText}
          errorStyle={errorStyle}/>
      </center>
    }

    const renderLabel = (text, style) => {
      return <div style={style}>
        {text}
      </div>
    }

    const renderAddButton = (text, style) => {
      return <div style={style}>
        <div style={{ float: 'right', paddingRight: '30px' }}>
          <FlatButton label={text} primary={true} ><AddIcon/></FlatButton>
        </div>
      </div>
    }

    const fieldContainerStyle = {
      position: 'relative'
    }

    const renderUi = () => {
      if (this.props.guestlist.get('records') === undefined) {
        this.loading();
      } else {
        var width = window.innerWidth;
        var height = (window.innerHeight) - 160;
        return <div>
          <Search currentLocation={this.props.currentLocation}/>
          <div style={{ float: 'left', marginTop: '0px', marginLeft: '0px', width: (width * .20) }}>
            <div className="current-customer-panel search">

              <div className="cancel"><SearchIcon color="#fff"/></div>
            </div>
            <Subheader style={{ marginLeft: '55px', color: '#fff' }}>{this.state.count} Confirmed guests</Subheader>
            <Divider className="line-divider"/>
            <GuestList onChange={this.handleListSelect.bind(this) }
              records={this.props.guestlist.get('records') }
              height={height}/>
          </div>

          <div id="container">
            <div layout="row" className="content">
              <div className="form-panel">
                <div id='inner-form' style={{ marginLeft: 'auto', marginRight: 'auto', overflow: 'hidden', textAlign: 'left', height: '70%', width: '80%' }}>
                  <div style={fieldContainerStyle}>
                    <br/>
                    <Card style={{ backgroundColor: '#d1d1d1' }}>

                      {Avatar() }
                      {renderLabel(this.state.firstName + ' ' + this.state.lastName, fieldFullNameStyle) }
                      {renderLabel(this.state.company, fieldCompanyStyle) }

                      <br />

                      <Divider className="line-divider"/>
                      <br />
                      {renderLabel('Contact details:', fieldHeadingStyle) }


                      {renderfNameTextField('Enter email address', 'Email address',
                        this.state.emailAddress, 'emailAddress', this.state.emailaddress_err) }

                      {renderfNameTextField('Enter mobile number', 'Mobile Number',
                        this.state.contactNumber, 'contactNumbers', this.state.contactNumber) }

                      {renderAddButton('Add number', addNumberButtonStyle) }

                      <div style={addNumberButtonStyle}>
                        <div style={{ float: 'right', paddingRight: '30px' }}>
                          <FlatButton label='Add email' primary={true}  onClick={this.addEmailAddress.bind(this) }>
                            <AddIcon/>
                          </FlatButton>
                        </div>
                      </div>

                      <br /><br /><br />
                      <Divider className="line-divider"/>

                      <div hidden={this.state.canSave} style={addNumberButtonStyle}>
                        <div  style={{ display: 'flex', justifyContent: 'center', marginBottom: '15px' }}>
                          <FlatButton label='Save details' primary={true} style={buttonStyle} onClick={this.saveDetails.bind(this) }/>
                        </div>
                      </div>
                      <br />
                    </Card>
                    <br />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      }
    }

    return (
      <MuiThemeProvider muiTheme={muiTheme}>


        <div >
          {renderUi() }

        </div>
      </MuiThemeProvider >

    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    guestlist: state.guests
  };
}

function mapDispatchToProps(dispatch) {
  let bindings = bindActionCreators({ loadRecords, addContactNumber }, dispatch);
  return bindings
}

export default connect(mapStateToProps, mapDispatchToProps)(MainLayout)
