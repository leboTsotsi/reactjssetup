import React, { Component, PropTypes } from 'react';
import { Provider } from 'react-redux';
import routes from '../routes';
import DevTools from './DevTools';
import { Router } from 'react-router';

let counter = 0;

export default class Root extends Component {
  
  render() {
    const { store, history } = this.props;

    console
    return (
      <Provider store={store}>
        <div>
          <Router  key={ counter } history={history} routes={routes(store)} />
          <DevTools />
        </div>
      </Provider>
    )
  }
}

if (module.hot) {
    counter++;
    module.hot.accept();
}

Root.propTypes = {
  store: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired
}