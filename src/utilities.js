export function isEmailInvalid(email) {
    let reg = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|property|tech|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/;
    let isEmailInvalid = !reg.test(email.trim().toLowerCase());

    return isEmailInvalid;
}

export function isCellInvalid(cell) {
    let phoneRegex = /^((?:\+27|27)|0)(6|7|8)(\d{8})$/;
    let isPhoneInvalid = !phoneRegex.test(cell);

    return isPhoneInvalid;
}