export const LOAD_RECORDS = 'LOAD_RECORDS';
export function loadRecords(payload) {
    return {
        type: LOAD_RECORDS,
        payload
    }
}

export const ADD_CONTACT_NUMBER = "ADD_CONTACT_NUMBER";
export function addContactNumber(payload) {
    return {
        type: ADD_CONTACT_NUMBER,
        payload
    }
}