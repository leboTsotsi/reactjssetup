export const SET_NEXT_ROUTE = "SET_NEXT_ROUTE";
export const RESET_NEXT_ROUTE = "RESET_NEXT_ROUTE";

export function setNextRoute(nextRoute: string) {
  return {
    type: SET_NEXT_ROUTE,
    nextRoute: nextRoute
  }
}

export function resetNextRoute() {
  return {
    type: RESET_NEXT_ROUTE
  }
}

export function dispatchNextRoute(nextRoute: string) {
	return (dispatch: any, getState: any) => {
		dispatch(setNextRoute(nextRoute))
	}
}