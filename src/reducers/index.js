import { routerReducer as routing } from 'react-router-redux';
import { merge } from 'lodash';
import { combineReducers } from 'redux';
import { fromJS } from 'immutable';
import { guests} from './guests';
import { routes } from './routes';

const appReducer = combineReducers({
    routing,
    routes,
    guests
});

const rootReducer = (state, action) => {
    return appReducer(state, action);
}

export default rootReducer;