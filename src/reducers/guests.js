import * as RecordActionTypes from '../actions/records';
import { merge } from 'lodash';
import Immutable from 'immutable';
import {REHYDRATE} from 'redux-persist/constants'

export function guests(state = Immutable.fromJS({ dataset: {} }), action) {
    switch (action.type) {
        case RecordActionTypes.LOAD_RECORDS:
            return state.merge(Immutable.fromJS(action.payload));
        case RecordActionTypes.ADD_CONTACT_NUMBER:
            return  state.merge(Immutable.fromJS(action.payload));
        case REHYDRATE:
            return Immutable.fromJS({});
        default:
            return state;
    }
}