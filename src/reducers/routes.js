import {fromJS} from 'immutable';
import * as RoutesActions from '../actions/routes';
import {REHYDRATE} from 'redux-persist/constants'

export function routes(state = fromJS({ next: undefined }), action) {
  switch (action.type) {
    case RoutesActions.SET_NEXT_ROUTE:
      if (action.nextRoute !== '/') {
        return state.merge({ previous: action.nextRoute, next: action.nextRoute });
      } else {
        return state.merge({ next: action.nextRoute });
      }

    case RoutesActions.RESET_NEXT_ROUTE:
      return state.merge({ next: undefined });
    default:
      return state.merge({ previous: '/', next: '/' });
  }
}