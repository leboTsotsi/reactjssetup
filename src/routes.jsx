import React from 'react'
import { Route, IndexRoute, browserHistory } from 'react-router'
import _ from 'lodash';

import MainLayout from './containers/MainLayout';
import Home from './containers/Home'

export const routes = (store) => {
    return <Route path="/" component={MainLayout}>
        <IndexRoute component={MainLayout}/>
    </Route>
}
export default routes;