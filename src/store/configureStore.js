import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import createLogger from 'redux-logger'
import reducers from '../reducers'
import DevTools from '../containers/DevTools'
import {persistStore, createTransform, storages, autoRehydrate} from 'redux-persist';
import { asyncLocalStorage } from "redux-persist/storages";
import Immutable from 'immutable';
import _ from 'lodash';
import {REHYDRATE} from 'redux-persist/constants'
import createActionBuffer from 'redux-action-buffer'

function api() {
  let payload = {lebo: 'tsotetsi'};
  return ({
    type: successType,
    reqPayload: payload
  })
}

export default function configureStore(initialState) {
  const createPersistentStore = compose(
    applyMiddleware(thunk, createActionBuffer(REHYDRATE)),
    autoRehydrate(),
    window.devToolsExtension ? window.devToolsExtension() : f => f
  )(createStore);

  const store = createPersistentStore(reducers, initialState);
  persistStore(store, { storage: asyncLocalStorage, blacklist: ['routing', 'routes'] })

  if (module.hot) {
    module.hot.accept('../reducers', () => {
      const nextRootReducer = require('../reducers').default
      store.replaceReducer(nextRootReducer)
    })
  }
  return store
}