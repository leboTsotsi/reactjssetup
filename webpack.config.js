var path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var HTMLWebpackPluginConfig = new HtmlWebpackPlugin({
  template: __dirname + '/src/index.html',
  filename: 'index.html',
  inject: 'body'
});

module.exports = {
  entry: {
    javascript: './src/index.jsx',
  },

  output: {
    filename: 'punk.js',
    path: __dirname + '/dist'
  },
  resolve: {
    extensions: ['.js', '.jsx', '.json', '.css']
  },

  plugins: [HTMLWebpackPluginConfig],
  devServer: {
    inline: true,
    port: 7777
  },

  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        query: {
          presets: ['es2015', 'react']
        }
      },
      { test: /\.xml$/, loader: 'xml-loader' },
      { test: /\.scss$/, loaders: ["style-loader", "css", "sass"] },
      { test: /\.css$/, loader: "style-loader!css-loader" }
    ]
  }
}
