## Tech stack
* [Babel](https://babeljs.io/) for that next level Javascript
* [React](https://facebook.github.io/react/) for UI rendering
* [React router](https://github.com/reactjs/react-router-tutorial) For routing
* [React Developer Tools](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi) for better development flow
* [Redux](http://redux.js.org/) Application architecture library to make working with state easy
* [Redux Devtools](https://github.com/gaearon/redux-devtools) So much awesome to help developing redux apps
* [Webpack](http://webpack.github.io/) - for module loading
* [Material UI](http://www.material-ui.com/) - for that material goodness

## Setup
npm upgrade

npm install webpack --global

npm install webpack-dev-server --global

npm install react-redux

npm install react-redux-router@3.0.2

npm install redux

npm install redux-action-buffer

npm install redux-devtools

npm install redux-persist

npm install xml-loader

npm install redux-thunk

npm install immutable

npm install material-ui

npm install lodash

npm install redux-mock-store

npm install redux-devtools-dock-monitor

npm install redux-devtools-log-monitor

npm install html-webpack-plugin

npm install babel-preset-react

npm install material-ui@next